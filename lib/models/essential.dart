class Essential {
  int id;
  int topic;
  String vocabulary;
  String vocalizaton;
  String explanation;
  String kind;
  String translate;
  String example;
  String extranslate;
  int favourite;

  Essential({
    this.id,
    this.topic,
    this.vocabulary,
    this.vocalizaton,
    this.explanation,
    this.kind,
    this.translate,
    this.example,
    this.extranslate,
    this.favourite,
  });
  factory Essential.fromMap(Map<String, dynamic> json) => new Essential(
        id: json["id"],
        topic: json["topic"],
        vocabulary: json["vocabulary"],
        vocalizaton: json["vocalization"],
        explanation: json["explanation"],
        kind: json["kind"],
        translate: json["translate"],
        example: json["example"],
        extranslate: json["ex_translate"],
        favourite: json["favourite"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "topic": topic,
        "vocabulary": vocabulary,
        "vocalization": vocalizaton,
        "explanation": explanation,
        "kind": kind,
        "translate": translate,
        "example": example,
        "ex_translate": extranslate,
        "favourite": favourite,
      };
}
