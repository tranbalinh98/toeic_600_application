import 'package:flutter_tts/flutter_tts.dart';

class TextToSpeechService {
  final double _volume = 1;
  final double _pitch = 1.0;
  final double _rate = 0.5;
  final String _language = 'en-US';

  static FlutterTts _flutterTts;
  TextToSpeechService._();
  static final TextToSpeechService tts = TextToSpeechService._();

  Future<FlutterTts> get flutterTts async {
    if (_flutterTts != null) {
      return _flutterTts;
    }
    _flutterTts = await _initTts();
    return _flutterTts;
  }

  Future<FlutterTts> _initTts() async {
    FlutterTts tts = new FlutterTts();
    await tts.setVolume(_volume);
    await tts.setSpeechRate(_rate);
    await tts.setPitch(_pitch);
    await tts.setLanguage(_language);

    return tts;
  }
}
