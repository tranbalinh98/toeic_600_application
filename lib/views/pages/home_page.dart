import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toeic_600/viewmodels/home_viewmodel.dart';
import 'package:toeic_600/views/widgets/topic_item.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<HomeViewModel>(
      create: (
        _,
      ) =>
          HomeViewModel(),
      child: Consumer<HomeViewModel>(
        builder: (_, model, child) => Scaffold(
          body: Center(
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: 0,
                  child: Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).padding.top),
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height / 2,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          Colors.blueAccent,
                          Colors.lightBlueAccent,
                          Colors.white,
                          Colors.white,
                        ],
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.all(20),
                          width: MediaQuery.of(context).size.width,
                          child: Text(
                            'Toeic 600',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 40,
                                fontFamily: model.fontAvenirDemiBold),
                          ),
                        ),
                        Container(
                          height: 40,
                          width: 300,
                          decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              color: Colors.white38),
                          child: TextField(
                            onTap: () => model.search(context),
                            autofocus: false,
                            readOnly: true,
                            decoration: InputDecoration(
                              contentPadding: EdgeInsets.only(left: 30, top: 5),
                              suffixIcon: Icon(
                                Icons.search,
                                color: Colors.black,
                              ),
                              hintText: 'Search',
                              border: InputBorder.none,
                            ),
                          ),
                        ),
                        Spacer(),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  top: (MediaQuery.of(context).size.height * 0.25) +
                      MediaQuery.of(context).padding.top,
                  child: Container(
                      // padding:EdgeInsets.only(top:30),
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.75,
                      // color: Colors.white,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: model.isBusy
                                ? Center(
                                    child: SizedBox(
                                      height: 50,
                                      width: 50,
                                      child: CircularProgressIndicator(),
                                    ),
                                  )
                                : ListTopic(topics: model.topics),
                          ),
                        ],
                      )),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
