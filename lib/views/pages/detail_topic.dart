import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toeic_600/models/topic.dart';
import 'package:toeic_600/viewmodels/detail_topic_viewmodel.dart';
import 'package:toeic_600/views/widgets/detail_topic_list.dart';

class DetailTopic extends StatelessWidget {
  final Topic topic;
//  final List<Essential> essentials;
  DetailTopic({
    Key key,
    @required this.topic,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<DetailTopicViewModel>(
      create: (
        _,
      ) =>
          DetailTopicViewModel(this.topic.id),
      child: Consumer<DetailTopicViewModel>(
        builder: (_, model, child) => Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.blue,
            title: Text(this.topic.name),
            leading: IconButton(
              onPressed: () => {Navigator.pop(context)},
              icon: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            actions: <Widget>[
              IconButton(
                onPressed: () => model.search(context),
                icon: Icon(Icons.search),
              ),
            ],
          ),
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: model.isBusy
                ? Center(
                    child: SizedBox(
                      height: 50,
                      width: 50,
                      child: CircularProgressIndicator(),
                    ),
                  )
                : DetailTopicList(essentials: model.essentialsByTopic),
          ),
        ),
      ),
    );
  }
}
