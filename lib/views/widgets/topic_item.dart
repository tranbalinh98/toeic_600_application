import 'package:flutter/material.dart';
import 'package:toeic_600/models/topic.dart';
import 'package:toeic_600/views/pages/detail_topic.dart';

class ListTopic extends StatelessWidget {
  final List<Topic> topics;
  ListTopic({
    Key key,
    @required this.topics,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
      ),
      itemCount: topics.length,
      itemBuilder: _item,
    );
  }

  Widget _item(BuildContext context, int index) {
    return InkWell(
      onTap: () => _moveDetail(context, topics[index]),
      child: Container(
        margin: EdgeInsets.only(bottom: 10, right: 10, left: 10),
        decoration: BoxDecoration(
          color: Colors.yellow,
          boxShadow: [
            BoxShadow(
              color: Colors.black12,
              blurRadius: 20.0,
              spreadRadius: 2.0,
              offset: Offset(
                0.0,
                0.0,
              ),
            ),
          ],
          borderRadius: BorderRadius.all(
            Radius.circular(10),
          ),
        ),
        child: Column(
          children: <Widget>[
            Image(
              image: AssetImage('assets/images/img_Topic.png'),
            ),
            Container(
              width: 200,
              height: 50,
              padding: EdgeInsets.only(left: 10, right: 10, top: 10),
              decoration: BoxDecoration(
                  color: Colors.white54,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(10),
                    bottomRight: Radius.circular(10),
                  )),
              child: Text(
                topics[index].name,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 16,
                    fontWeight: FontWeight.w800),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _moveDetail(
    BuildContext context,
    Topic topic,
  ) {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (_) => DetailTopic(
          topic: topic,
        ),
      ),
    );
  }
}
