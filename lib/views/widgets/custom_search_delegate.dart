import 'package:flutter/material.dart';
import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/models/topic.dart';
import 'package:toeic_600/views/widgets/detail_topic_list.dart';

class CustomSearchDelegate extends SearchDelegate {
  List<Essential> essentials;
  List<Essential> essentialsFilter;
  List<Topic> topics;
  bool isBusy = false;
//  SqlHelper helper;

  CustomSearchDelegate(List<Essential> essentials) {
//    setSearchData();
    this.essentials = essentials;
//    helper = new SqlHelper();
    essentialsFilter = new List<Essential>();
    getSearchData();
  }
  void getSearchData() async {
    this.essentialsFilter = essentials;
  }

  void getFilter() {
    if (query.isEmpty) {
      essentialsFilter = essentials;
    } else {
      essentialsFilter = new List<Essential>();
      essentials.forEach((element) {
        if (element.vocabulary.contains(query)) {
          this.essentialsFilter.add(element);
        }
      });
    }
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    getFilter();
    return isBusy
        ? (Center(
            child: SizedBox(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(),
            ),
          ))
        : (essentialsFilter.length > 0
            ? DetailTopicList(essentials: this.essentialsFilter)
            : Center(
                child: Text('No Result'),
              ));
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(
        onPressed: () => {query = ''},
        icon: Icon(
          Icons.clear,
          color: Colors.black,
        ),
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.arrow_back,
        color: Colors.black,
      ),
      onPressed: () => {close(context, null)},
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    getFilter();
    return isBusy
        ? Center(
            child: SizedBox(
              height: 50,
              width: 50,
              child: CircularProgressIndicator(),
            ),
          )
        : DetailTopicList(essentials: this.essentialsFilter);
  }
}
