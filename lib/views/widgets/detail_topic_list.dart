import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/viewmodels/item_essential_viewmodel.dart';

class DetailTopicList extends StatelessWidget {
  final List<Essential> essentials;
  DetailTopicList({
    Key key,
    @required this.essentials,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: essentials.length,
      itemBuilder: _item,
    );
  }

  Widget _item(BuildContext context, int index) {
    return ChangeNotifierProvider<ItemEssentialViewModel>(
      create: (
        _,
      ) =>
          ItemEssentialViewModel(),
      child: Consumer<ItemEssentialViewModel>(
        builder: (_, model, child) => ExpansionTile(
          subtitle: Text(
            essentials[index].kind,
            style: TextStyle(
                fontSize: 12, fontWeight: FontWeight.w900, color: Colors.blue),
          ),
          trailing: SizedBox(),
          leading: IconButton(
            icon: Icon(Icons.layers, color: Colors.blue),
            onPressed: null,
          ),
          title: Text(
            essentials[index].vocabulary,
            style: TextStyle(fontSize: 16),
          ),
          children: <Widget>[
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(20),
              decoration: BoxDecoration(
                color: Colors.black12,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        text: 'Phiên âm: ',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                        children: [
                          TextSpan(
                            text: essentials[index].vocalizaton,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'Loại: ',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                        children: [
                          TextSpan(
                            text: essentials[index].kind,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'Phiên dịch: ',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                        children: [
                          TextSpan(
                            text: essentials[index].translate,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'Sử dụng: ',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                        children: [
                          TextSpan(
                            text: essentials[index].explanation,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                        ]),
                  ),
                  RichText(
                    text: TextSpan(
                        text: 'Ví dụ: ',
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.w700,
                            fontSize: 16),
                        children: [
                          TextSpan(
                            text: essentials[index].example,
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w400),
                          ),
                          TextSpan(
                            text: '(' + essentials[index].extranslate + ').',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.w300),
                          ),
                        ]),
                  ),
                  Row(
                    children: <Widget>[
                      Spacer(),
                      IconButton(
                        icon: Icon(Icons.volume_up, color: Colors.black),
                        onPressed: () =>
                            model.speak(essentials[index].vocabulary),
                      ),
                      IconButton(
                        icon: essentials[index].favourite > 0
                            ? Icon(
                                Icons.favorite,
                                color: Colors.red,
                              )
                            : Icon(
                                Icons.favorite_border,
                                color: Colors.red,
                              ),
                        onPressed: () =>
                            model.setEssentialFavourite(essentials[index]),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
