import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/viewmodels/base_viewmodel.dart';
import 'package:toeic_600/views/widgets/custom_search_delegate.dart';

class DetailTopicViewModel extends BaseViewmodel {
  List<Essential> essentialsByTopic;
  DetailTopicViewModel(int topic) {
    setDbEssentialByTopic(topic);
  }
  void setDbEssentialByTopic(int topic) async {
    isBusy = true;
    essentialsByTopic = await helper.getEssentialByTopic(topic);
    isBusy = false;
    notifyListeners();
  }

  void search(BuildContext context) {
    showSearch(
        context: context, delegate: CustomSearchDelegate(essentialsByTopic));
  }

  void speak(String voiceText) {
    textToSpeech.speakText(voiceText);
  }
}
