import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/models/topic.dart';

import 'package:toeic_600/viewmodels/base_viewmodel.dart';
import 'package:toeic_600/views/widgets/custom_search_delegate.dart';

class HomeViewModel extends BaseViewmodel {
  String dbContent = 'vvv';
  List<Essential> essentials;
  List<Topic> topics;
  bool isSearch;
  Widget title;

  HomeViewModel() {
    isSearch = false;
    setDbTopic();
    setDbEssential();
  }
  void setDbEssential() async {
    isBusy = true;
    essentials = await helper.getEssential();
    isBusy = false;
    notifyListeners();
  }

  void setDbTopic() async {
    isBusy = true;
    topics = await helper.getTopic();
    isBusy = false;
    notifyListeners();
  }

  void search(BuildContext context) {
    showSearch(
        context: context, delegate: CustomSearchDelegate(this.essentials));
  }

  final String fontAvenirDemiBold = 'AvenirDemiBold';
  final String fontAvenirMediumFont = 'AvenirMediumFont';
  final String fontAvenirRegularfont = 'AvenirRegularFont';
}
