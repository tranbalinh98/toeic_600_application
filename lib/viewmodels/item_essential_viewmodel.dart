import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/viewmodels/base_viewmodel.dart';

class ItemEssentialViewModel extends BaseViewmodel {
  void setEssentialFavourite(Essential essential) async {
    isBusy = true;
    if (essential.favourite == 0) {
      essential.favourite = 1;
    } else {
      essential.favourite = 0;
    }
    await helper.isFavourite(essential);
    isBusy = false;
    notifyListeners();
  }

  void speak(String voiceText) {
    if (isBusy) {
      return;
    }
    isBusy = true;
    textToSpeech.speakText(voiceText);
    isBusy = false;
  }
}
