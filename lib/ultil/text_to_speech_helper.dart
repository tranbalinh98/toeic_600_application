import 'package:flutter_tts/flutter_tts.dart';
import 'package:toeic_600/services/text_to_speech_service.dart';

class TextToSpeechHelper {
  Future speakText(String voiceText) async {
    var flutterTts = await TextToSpeechService.tts.flutterTts;
    if (voiceText != null && voiceText.isNotEmpty) {
      await flutterTts.speak(voiceText);
    }
  }
}
