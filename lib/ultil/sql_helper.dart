import 'package:toeic_600/services/data_provider.dart';
import 'package:toeic_600/models/essential.dart';
import 'package:toeic_600/models/topic.dart';

class SqlHelper {
  static final String dbNAme = 'toeic600.db';
  static final String essentialTable = 'ESSENTIAL';
  static final String topicTable = 'TOPIC';

  Future<List<Essential>> getEssential() async {
    final db = await DataProvider.db.database;
    var res = await db.query(essentialTable);
    print('SqlHelper:' + res.toString());
    List<Essential> essentials =
        res.isNotEmpty ? res.map((e) => Essential.fromMap(e)).toList() : [];
    print('SqlHelper: mapping essential success');
    return essentials;
  }

  Future<List<Essential>> getEssentialByTopic(int topic) async {
    final db = await DataProvider.db.database;
    var res =
        await db.query(essentialTable, where: "topic = ?", whereArgs: [topic]);
    print('SqlHelper/GetEssential by ID:' + res.toString());
    List<Essential> essentials =
        res.isNotEmpty ? res.map((e) => Essential.fromMap(e)).toList() : [];
    print('SqlHelper: mapping essential success');
    return essentials;
  }

  Future<List<Essential>> getEssentialByQuery(String query) async {
    final db = await DataProvider.db.database;
    var res = await db.query(
      essentialTable,
      where: " vocabulary like ? ",
      whereArgs: [query],
    );
    print('SqlHelper/GetEssential by ID:' + res.toString());
    List<Essential> essentials =
        res.isNotEmpty ? res.map((e) => Essential.fromMap(e)).toList() : [];
    print('SqlHelper: mapping essential success');
    return essentials;
  }

  isFavourite(Essential essential) async {
    final db = await DataProvider.db.database;
    var res = await db.update(essentialTable, essential.toMap(),
        where: "id = ?", whereArgs: [essential.id]);
    return res;
  }

  Future<List<Topic>> getTopic() async {
    final db = await DataProvider.db.database;
    var res = await db.query(topicTable);
    print('SqlHelper:' + res.toString());
    List<Topic> topics =
        res.isNotEmpty ? res.map((e) => Topic.fromMap(e)).toList() : [];

    print('SqlHelper: mapping topic success');
    return topics;
  }
//  Future

}
